#!/usr/bin/env python3.10
import argparse
import time
from datetime import timedelta, datetime

from daemon import pidfile
import daemon
import logging

import os
import subprocess
from pathlib import Path

# import library to set up a time interval
from apscheduler.schedulers.blocking import BlockingScheduler


def start_daemon(pidf, logf):
    # This launches the daemon in its context
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler('daemon.log')
    logger.addHandler(handler)

    rootpath = os.path.dirname(os.path.abspath(__file__))
    # XXX pidfile is a context
    with daemon.DaemonContext(
            stdout=handler.stream,
            stderr=handler.stream,
            working_directory=rootpath,
            umask=0o002,
            pidfile=pidfile.TimeoutPIDLockFile(pidf),
            files_preserve=[handler.stream]
    ) as context:
        change_type('audio_storage')


def change_type(path_to_dir):
    entries = Path(path_to_dir)
    for entry in entries.iterdir():
        # if the element in directory is dir too, start recursion
        if entry.is_dir():
            change_type("/".join([path_to_dir, entry.name]))
        # if the element in .wav type, encode as .mp3
        elif entry.name.endswith('.wav'):
            wav = '/'.join([path_to_dir, entry.name])
            cmd = "lame --preset insane '%s'" % wav
            subprocess.call(cmd, shell=True)
            os.remove(wav)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Daemon Service")
    parser.add_argument('-p', '--pid-file', default='daemon.pid')
    parser.add_argument('-l', '--log-file', default='daemon.log')

    args = parser.parse_args()

    while 1:
        start_daemon(pidf=args.pid_file, logf=args.log_file)

        dt = datetime.now() + timedelta(seconds=5)
        dt = dt.replace(minute=30)

        while datetime.now() < dt:
            time.sleep(1)